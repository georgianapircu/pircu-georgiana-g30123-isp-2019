import org.junit.Test;
import pircu.georgiana.lab6.ex2.Bank;

import static org.junit.Assert.assertEquals;

public class ex2Test {
    @Test
    public void Test(){
        Bank myBank = new Bank();
        myBank.addAccount ("Owner1",700);
        myBank.addAccount ("Owner2",890);
        myBank.addAccount ("Owner3",329);
        myBank.addAccount ("Owner4",100.65);
        assertEquals (4,myBank.getAllAccounts ().size (),1.0);

    }

}

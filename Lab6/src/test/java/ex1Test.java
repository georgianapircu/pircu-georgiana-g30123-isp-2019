import org.junit.Test;
import pircu.georgiana.lab6.ex2.BankAccount;

import static org.junit.Assert.assertEquals;

public class ex1Test {
    @Test
    public void Test(){
        BankAccount myAAccount = new BankAccount ("Elena",890);
        assertEquals (true,myAAccount.equals (new BankAccount ("Elena",400)));
        assertEquals (false,myAAccount.equals (new BankAccount ("Elena",990)));
        assertEquals (false,myAAccount.equals (new BankAccount ("Elena",342)));
    }

}

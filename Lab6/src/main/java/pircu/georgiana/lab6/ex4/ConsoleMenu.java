package pircu.georgiana.lab6.ex4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main (String[] args) {
        int opt = -1;
        Dictionary myDictionary = new Dictionary ();
        while(opt != 6){
            System.out.println ("1)Add_Word\n2)Get_Definition\n3)Get_All_Definitions\n4)Get_All_Words\n5)Exit");
            Scanner citire = new Scanner(System.in);
            System.out.println ("Alegeti o optiune: ");
            opt =citire.nextInt ();
            switch (opt){
                case 1: myDictionary.readWordandDefinition ();
                    break;
                case 2: myDictionary.readWord ();
                    break;
                case 3: myDictionary.getAllDefinitons ();
                    break;
                case 4 :myDictionary.getAllWords ();
                    break;
                case 5: return;

                default:
                    System.out.println ("Optiune invalida");
                    break;
            }
        }
    }
}


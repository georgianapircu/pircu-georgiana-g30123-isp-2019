package pircu.georgiana.lab6.ex3;

import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> sortByOwner = new TreeSet<>(Comparator.comparing (BankAccount::getOwner));
    private TreeSet<BankAccount> sortByBalance = new TreeSet<> (Comparator.comparingDouble (BankAccount::getBalance));

    public void addAccount(String owner,double balance){
        BankAccount myAccount = new BankAccount (owner,balance);
        sortByOwner.add (myAccount);
        sortByBalance.add (myAccount);
    }
    public void printAccounts() {
        for (BankAccount i : sortByBalance)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void printAccounts(double minRange, double maxRange) {
        for (BankAccount i : sortByBalance)
            if (i.getBalance() >= minRange && i.getBalance() <= maxRange)
                System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void getAllAcounts() {
        for (BankAccount i : sortByOwner)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }

}

package pircu.georgiana.lab5.ex1;

public class Circle extends  Shape{
    double radius;

    public Circle () {
        radius=1.0;
    }

    public Circle (double radius) {
        this.radius = radius;
    }

    public Circle (String color, boolean filled, double radius) {
        super (color, filled);
        this.radius = radius;
    }

    public double getRadius () {
        return radius;
    }

    public void setRadius (double radius) {
        this.radius = radius;
    }
    public double getArea(double radius){
        return Math.PI*this.radius*this.radius;
    }
    public double getPerimeter(double radius) {
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString () {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                ", fillded=" + filled +
                '}';
    }

}

package aut.utcluj.isp.ex2;

import java.util.Objects;

/**
 * @author stefan
 */
public class Notebook extends Equipment {
    private String osVersion;
    private String serialNumber;
    private String name;

    public Notebook(String name, String serialNumber, String osVersion) {
        super(name,serialNumber);
        this.osVersion=osVersion;
    }

    public String getOsVersion() {

        return osVersion;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notebook notebook = (Notebook) o;
        return Objects.equals(osVersion, notebook.osVersion) &&
                Objects.equals(serialNumber, notebook.serialNumber) &&
                Objects.equals(name, notebook.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(osVersion, serialNumber, name);
    }
    public String start()
    {
        return "Notebook " + getName() + " started";
    }
}

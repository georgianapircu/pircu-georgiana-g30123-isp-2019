package pircu.georgiana.lab8.ex3;

import java.util.ArrayList;

class Controler{

    String stationName;

    ArrayList<Controler> list2 = new ArrayList<Controler>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String station) {
        stationName = station;
    }

    void setNeighbourController(Controler c){
        list2.add(c);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }


    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();

                for(Controler c:list2){
                    if(t.getDestination().equals(c.stationName)){
                        //check if there is a free segment
                        int id = c.getFreeSegmentId();
                        if(id==-1){
                            System.out.println("Train "+t.name+" from station "+stationName+" could not be send to "+c.stationName+". No segment available!");
                            return;
                        }
                        //send train
                        System.out.println("Train "+t.name+" leaves statiion "+stationName +" to station "+c.stationName);
                        segment.departTRain();
                        c.arriveTrain(t,id);
                    }

                }
            }//.for
        }
    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){

            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }


        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public void displayStationState(){
        System.out.println("=== STATION "+stationName+" ===");
        for(Segment s:list){
            if(s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
            else
                System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
        }
    }
}
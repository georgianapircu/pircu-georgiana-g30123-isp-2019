package pircu.georgiana.lab11.ex1;


interface Observable {
    void update (Observable t, Object o);
}

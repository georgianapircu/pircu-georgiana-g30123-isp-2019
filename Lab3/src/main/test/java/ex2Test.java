import org.junit.Test;
import pircu.georgiana.lab3.ex2.Circle;

import static org.junit.Assert.assertEquals;

public class ex2Test {
    @Test
    public void Test(){
        Circle myCircle = new Circle(5);
        assertEquals(5, myCircle.getRadius(),1.0);
        assertEquals(Math.PI*Math.pow(5,2),myCircle.getArea(),1.0);


    }
}

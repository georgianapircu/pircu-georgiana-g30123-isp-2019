import org.junit.Test;
import pircu.georgiana.lab3.ex3.Author;

import static org.junit.Assert.assertEquals;

public class ex3Test {
    @Test
    public void Test(){
        Author myAuthor= new Author("Autor1", "email",'m');
        assertEquals("Autor1 m at email",myAuthor.toString());
    }
}

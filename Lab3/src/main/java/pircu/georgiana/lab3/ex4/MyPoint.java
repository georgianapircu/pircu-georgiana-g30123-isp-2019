package pircu.georgiana.lab3.ex4;

public class MyPoint {
    int x;
    int y;

    public MyPoint () {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX () {
        return x;
    }

    public int getY () {
        return y;
    }

    public void setX (int x) {
        this.x = x;
    }

    public void setY (int y) {
        this.y = y;
    }

    public void setXY (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString () {
        return "(" + this.x + "," + this.y + ")";
    }

    public double distance (int x, int y) {
        return Math.sqrt ((this.x - x) ^ 2 + (this.y - y) ^ 2);
    }

    public double distance (MyPoint newPoint) {
        return Math.sqrt ((this.x - newPoint.x) ^ 2 + (this.y - newPoint.y) ^ 2);
    }

}

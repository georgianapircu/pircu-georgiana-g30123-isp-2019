package pircu.georgiana.lab3.ex1;

public class Robot {
    int value;

    public Robot () {
        this.value = 1;
    }

    public void change (int k) {
        if (k >= 1) {
            this.value += k;
        }
    }

    @Override
    public String toString () {
        return "Valoarea curenta este: " + this.value;
    }
}


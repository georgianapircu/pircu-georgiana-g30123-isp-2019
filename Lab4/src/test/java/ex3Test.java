import org.junit.Test;
import pircu.georgiana.lab4.ex2.Author;
import pircu.georgiana.lab4.ex3.Book;

import static org.junit.Assert.assertEquals;

public class ex3Test {
    @Test
    public void Test(){
        Author myAuthor = new Author("Elena","georgianapircu@yahoo.com",'f');
        Book myBook = new Book("Carte1",myAuthor,35.5);
        assertEquals ("Book-Carte1 by  Elena(f) at georgianapircu@yahoo.com",myBook.toString ());


    }
}

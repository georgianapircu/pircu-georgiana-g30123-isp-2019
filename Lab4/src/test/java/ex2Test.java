import org.junit.Test;
import static org.junit.Assert.assertEquals;
import pircu.georgiana.lab4.ex2.Author;

public class ex2Test {
    @Test
    public void Test(){
        Author myAuthor = new Author ("Elena","georgianapircu@yahoo.com",'f');
        assertEquals ("Elena (f) at georgianapircu@yahoo.com",myAuthor.toString ());
    }
}

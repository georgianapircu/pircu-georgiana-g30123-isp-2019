package pircu.georgiana.lab4.ex6;

public class Circle extends Shape {
    double radius;

    public Circle () {
        radius=1.0;
    }

    public Circle (double radius) {
        this.radius = radius;
    }

    public Circle (String color, boolean filled, double radius) {
        super (color, filled);
        this.radius = radius;
    }

    public double getRadius () {
        return radius;
    }

    public void setRadius (double radius) {
        this.radius = radius;
    }
    public double getArea(double radius){
        return Math.PI*this.radius*this.radius;
    }
    public double getPerimeter(double radius) {
        return 2*Math.PI*this.radius;
    }
    public String toString(){
        return "A circle with radius = " + this.radius +" ,which is a subclass of "+super.toString ();
    }



}

package pircu.georgiana.lab4.ex6;

public class Square extends Rectangle {

    public Square () {
        super ();
        double side = 1.0;
    }

    public Square (double side) {
        super (side, side);
        side = side;
    }


    public void setWidth (double width) {
        width = width;
    }

    public void setLength (double length) {
        length = length;
    }

    public void setSide (double side) {
        super.setLength (side);
        super.setWidth (side);
    }

    public double getSide () {
        return super.getWidth ();
    }

    public double getArea () {
        return getSide () * getSide ();
    }

    public double getPerimeter () {
        return 4 * getSide ();
    }


    public String toString () {
        return "A square with side = " + getSide () + " which is a subclass of " + super.toString ();
    }

}

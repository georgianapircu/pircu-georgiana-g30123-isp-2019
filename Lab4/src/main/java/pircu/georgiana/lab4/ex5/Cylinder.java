package pircu.georgiana.lab4.ex5;

import pircu.georgiana.lab4.ex1.Circle;

public class Cylinder extends Circle {
    double height;

    public Cylinder () {
        this.height=1.0;
    }

    public Cylinder (double radius) {
        super (radius);
    }

    public Cylinder (double radius, double height) {
        super (radius);
        this.height = height;
    }

    public double getHeight () {
        return height;
    }
    public double getVolume(){
        return Math.PI* getRadius() *getRadius() * this.height;
    }
}

